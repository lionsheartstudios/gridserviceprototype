'use strict';

var _Grid = require('./Grid');

var _Grid2 = _interopRequireDefault(_Grid);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var express = require('express');
var app = express();
var url = require('url');
var WebSocketServer = require('ws').Server;


// a set of gridId->[WebSocket], ie, a set of all WebSocket clients that are on a given Grid (island)
var WebSocketTable = {};
// a set of gridId->Grid
var GridTable = {};

var wss = new WebSocketServer({ port: 4000 });

// if a new client connection comes with a gridId, we add it to the WebSocketTable against the given gridId.
// hence, the client has 'joined an island'.
// if not gridId was provided, we create a new Grid (island) and associate the client with it.
wss.on('connection', function (ws, req) {

    var gridId = null;
    var query = {};
    var _query = url.parse(req.url).query;
    if (_query) {
        _query.split('&').forEach(function (tok) {
            var tok = tok.split('=');
            query[tok[0]] = tok[1];
        });
        gridId = query.gridId;
    }

    // if the request came with a gridId
    if (gridId) {
        // if the provided gridId doesnt exist, error
        if (!WebSocketTable[gridId]) {
            ws.send(JSON.stringify({ error: 'invalid gridId ' + gridId }));
            ws.close();
            return;
        }
        // join the grid
        WebSocketTable[gridId].push(ws);
    }

    // if the request didnt come with a gridId, create one
    // @richard: this would perhaps never happen, since grid creation is done by GameServer
    if (!gridId) {
        // create a new Grid by generating a new gridId. The second param in the Grid constructor is a broadcast handler
        gridId = Math.floor(Math.random() * 90000) + 10000;
        GridTable[gridId] = new _Grid2.default(gridId, function (message) {
            WebSocketTable[gridId].forEach(function (_ws) {
                return _ws.send(JSON.stringify(message));
            });
        });
        // and add the websocket to the WebSocketTable
        WebSocketTable[gridId] = ws;
    }

    ws.on('message', function (message) {
        console.log('received: %s', message);
        // the message mush contain the gridId
        GridTable[gridId].sendToSCServer(message);
    });
});