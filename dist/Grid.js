'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _require = require('child_process'),
    spawn = _require.spawn;

var done = void 0;

/**
 * This class represents an Island, which is effectively a Grid with Objects on it.
 * It will be constructed by the GameServer Gateway, which would provide a GridId to the constructor().
 * The constructor is spins a new shell process, which runs the SCServerConsoleApp. Hence, we get one process per Grid.
 * All websocket messages that are sent to this Grid would be passed to the SCServerConsoleApp process,
 * and all responses from the process would be relayed back (or broadcasted) to all clients connected to this gridId.
 */

var Grid = function () {

    /**
     * construct a Grid. Upon receiving a result from the SCServerConsoleApp (error or otherwise),
     * call the provided broadcast() function to broadcast the result to all clients in the given gridId
     * @param gridId
     * @param broadcast
     */
    function Grid(gridId, broadcast) {
        _classCallCheck(this, Grid);

        this.gridId = gridId;

        this.process = spawn(__dirname + '/runSCServer.sh');

        this.process.stdout.on('data', function (stdout) {
            console.log(gridId + ' stdout: ' + stdout);
            broadcast(stdout);
        });

        this.process.stderr.on('data', function (stderr) {
            console.log(gridId + ' stderr: ' + stderr);
            broadcast({ error: stderr });
        });
    }

    /**
     * Send the passed command to the SCServerConsoleApp process
     * @param command
     */


    _createClass(Grid, [{
        key: 'sendToSCServer',
        value: function sendToSCServer(command) {
            this.process.stdin.write(command + '\n');
        }
    }]);

    return Grid;
}();

exports.default = Grid;