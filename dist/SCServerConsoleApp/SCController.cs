﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace SCServerConsoleApp
{
    class SCController
    {

        public static void broadcast(String message)
        {
            Console.WriteLine(message);
        }

        public static void Main(string[] args)
        {
            Console.WriteLine("SCConsoleApp is ready for commands");
            while (true)
            {
                string line = Console.ReadLine();
                //@aleksa ... to test my Dashboard, I am just echoing back the command I had just received 2sec later. This shiuld have been the new state.
                Task.Delay(2000).ContinueWith(t => SCController.broadcast(line));
            }
        }

    }
}
