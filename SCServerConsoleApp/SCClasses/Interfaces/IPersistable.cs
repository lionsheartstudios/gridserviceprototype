﻿using System;
namespace SCServer.SCClasses
{
    public interface IPersistable
    {
        string Id { get; set; }
        string Serialize();
        void Deserilaize(string JSON);
        void SetState(string JsonState);
        void ValidateCurrentState();
    }
}
