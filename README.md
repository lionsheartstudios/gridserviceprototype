##Introduction

This app demonstrates how we can run C# code from a Nodejs server. 

`app.js` manages the websocket connections originating from a game-client or sc-dashboard. It uses (or creates one if required) a `Grid` class to handle the `commands` from/to the clients.

`Grid.js` defines the `Grid` class, which encapsulates a shell instance (ie, a unix process) of the `SCServerConsoleApp`. The two communicate through `stdin` and `stdout`.

  
##Installation

1. Install `Node.js v8.x` or above on your machine. 

2. From the root folder, run `npm install`. This will install all the packages.

3. From the root folder, run `node app.js`. This will start the websockets server locally on port 4000.

##Design

```
1.   1 Island = 1 Grid Object = 1 SCConsoleApp process 
2.   The communication between GridService and SCConsoleApp is through JSON string over stdin and stdout. \n (end of line) is considered as the signal that the command is finished 
3.   Every Grid must had a gridId. This should come from the GameServer Gateway, but at the moment, if a gridId is not provided, a new gridId is generated.
```

##Updating the SCServerConsoleApp

The intention for the GridService is to provide a `container` to the game-designers to host and call their C# code on the GameServer. 

Currently, `SCServerConsoleApp` lives as a folder inside the `GridServicePrototype` project. The game designers can clone this repo, update the `SCServerConsoleApp` code, and push it back. They will have to, subsequently, logon to the AWS instance where the GridService is hosted, and pull the latest code.

Following are my suggestions to make this process cleaner:

1. Provide a simple web-console with an `uplaod-zip-code` option. The game-designers can test their stuff locally (its a console app, do they can run it using `dotnet run`), zip it up, and upload the code.

2. Trigger a `git pull` on the GridService AWS instance upon the game-designers doing a `git-push` from their local machine, ie, establish a `CI/CD pipeline` for automatic deployment.

