﻿using System;
namespace SCServer.SCClasses
{
    public interface IExecutable
    {
        bool Validate(Command command);
        void Execute(Command command);
    }
}
