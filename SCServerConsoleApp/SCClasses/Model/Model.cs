﻿using System;
namespace SCServer.SCClasses
{
    public abstract class Model : IPersistable, IExecutable
    {

        public string Id { get; set; }

        public Model(string _id)
        {
            Id = _id;
        }

        public void Deserilaize(string JSON)
        {
            SetState(JSON);
        }

        public string Serialize()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }

        public void SetState (string JSONState) {
            Newtonsoft.Json.JsonConvert.PopulateObject(JSONState, this);
        }

        public abstract void Execute(Command command);

        public abstract bool Validate(Command command);

        public abstract void ValidateCurrentState(); 
    }
}
