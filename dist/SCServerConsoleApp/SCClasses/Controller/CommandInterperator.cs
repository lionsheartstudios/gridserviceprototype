﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SCServerConsoleApp;

namespace SCServer.SCClasses
{
    public static class CommandInterperator
    {
        public static void ExecuteCommand(string syncCode, object commandObjectOrJSON)
        {

            Command runningCommand = null;

            if (commandObjectOrJSON is string stringCommand)
            {
                Command jsonCommand = Command.FromJSON(stringCommand);
                if (jsonCommand != null)
                {
                    runningCommand = jsonCommand;
                }
            }
            else if (commandObjectOrJSON is Command command)
            {
                SCServer.SendCommand(command);
                runningCommand = command;
            }
            else
            {
                throw new ArgumentException("Bad Json or command");
                //Error here, not a string or command
            }

            if (runningCommand == null)
                // bad command condition
                return;

            if (runningCommand.CommandType == CommandTypes.MOVE)
            {
                Model model = ModelRegistry.GetModel(runningCommand.Target);
                if (model != null)
                {
                    if (model.Validate(runningCommand))
                    {
                        model.Execute(runningCommand);
                    }
                }
                return;
            }

            if (runningCommand.CommandType == CommandTypes.SET_STATE)
            {
                if (runningCommand.Target == "GRID")
                {
                    //Grid.Init(runningCommand.Payload);
                    // broadcase new state
                    // TODO: Aleksa, I am simply relaying back the state received. Once done, you should replace this with Grid.toJSON()
                    Task.Delay(2000).ContinueWith(t => SCController.broadcast(runningCommand.Payload));
                }
            }
            return;
        }

    }


}