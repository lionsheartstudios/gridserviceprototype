﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SCServer.SCClasses
{
    public class Command
    {
        public string CommandId { get; set; }
        public string CommandType { get; set; }
        public string Payload { get; set; }
        public string Target { get; set; }

        public Command(string type, string target, string payload)
        {
            this.CommandId = Guid.NewGuid().ToString();
            this.CommandType = type;
            this.Target = target;
            this.Payload = payload;
        }

        public static Command FromJSON(string json)
        {
            Command command = (Command)JsonConvert.DeserializeObject<Command>(json);
            return command;
        }

        public string ToJSON()
        {
            throw new NotImplementedException();
        }

    }
}
