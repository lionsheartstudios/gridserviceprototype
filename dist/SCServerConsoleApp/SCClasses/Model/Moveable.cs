﻿using System;
using Newtonsoft.Json;
namespace SCServer.SCClasses
{
    public abstract class Movable: GridObject
    {
        public class MovePayload
        {
            readonly internal int newXPosition, newZPosition;
            public MovePayload(int newXPosition, int newZPosition)
            {
                this.newXPosition = newXPosition;
                this.newZPosition = newZPosition;
            }
        }


        public Movable(string _id, int x, int z, int sizeX, int sizeZ) : base(_id)
        {
            this.x = x;
            this.z = z;
            this.sizeX = sizeX;
            this.sizeZ = sizeZ;
        }



        private void Move(MovePayload movePayload)
        {
            if (Grid.CanMoveMovable(this, movePayload.newXPosition, movePayload.newZPosition))
            {
                Grid.ApplyChangeToArray(null, x, z, sizeX, sizeZ);
                x = movePayload.newXPosition;
                z = movePayload.newZPosition;
                Grid.ApplyChangeToArray(this, x, z, sizeX, sizeZ);
            }
        }

        private bool MoveValidate(MovePayload movePayload)
        {
            if (movePayload == null)
            {
                return false;
            }

            return (Grid.CanMoveMovable(this, movePayload.newXPosition, movePayload.newZPosition));
           
        }

        public override void Execute(Command command)
        {
            switch (command.CommandType)
            {
                case CommandTypes.MOVE:
                    Move(JsonConvert.DeserializeObject<MovePayload>(command.Payload));
                    break;
                case CommandTypes.SET_STATE:
                default:
                    break;
            }
        }

        public override bool Validate(Command command)
        {
            switch (command.CommandType)
            {
                case CommandTypes.MOVE:
                    return MoveValidate(JsonConvert.DeserializeObject<MovePayload>(command.Payload));
                default:
                    return false;
            }
        }

        public override void ValidateCurrentState()
        {
            
        }
    }
}
