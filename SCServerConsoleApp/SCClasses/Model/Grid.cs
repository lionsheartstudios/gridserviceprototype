﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SCServer.SCClasses
{
    /// <summary>
    /// Helper class to keep track of possibly overlapping buildings.
    /// </summary>
    public class Grid: Model
    {
     
        internal static Movable[,] moveables  = new Movable[40,50];

        public Grid(string _id) : base(_id) {
        }

        internal static bool CanMoveMovable(Movable movable, int newX, int newZ)
        {

            //iterate over the the grid positions the object wants to move into.
            for (int x = newX; x < newX + movable.sizeX; x++)
            {
                for (int z = newZ; z < newZ + movable.sizeZ; z++)
                {
                    //check for out of bounds errors
                    if (x < 0 || x >= moveables.GetLength(0) || z < 0 || z >= moveables.GetLength(1))
                    {
                        return false;
                    }

                    // check if another building occupies this spot (but not if our building occupies this spot)
                    if (moveables[x, z] != null && moveables[x, z] != movable)  {
                        return false;
                    }
                }
            }
            return true;
        }

        internal static void ApplyChangeToArray(Movable appliedChange, int newX, int newZ, int sizeX, int sizeZ)
        {
            for (int x = newX; x < newX + sizeX; x++)
            {
                for (int z = newZ; z < newZ + sizeZ; z++)
                {
                    if (x < 0 || x >= moveables.GetLength(0) || z < 0 || z >= moveables.GetLength(1))
                    {
                        continue;
                    }
                    moveables[x, z] = appliedChange;
                }
            }
        }

        public class GridFromCommand {
            public class XGrid {
                public List<Building> Buildings;
            }
            public XGrid Grid;
        }
        public static void Init(string initState){
            GridFromCommand gfc = (GridFromCommand)JsonConvert.DeserializeObject<GridFromCommand>(initState);
            foreach (Building building in gfc.Grid.Buildings)
            {
                addGridObject(building);
            }
        }

        internal static void addGridObject(GridObject go){
            // validate if can be added
            // add it the internal grid
            // reguster wtth ojectRegustry
            ModelRegistry.AddModel(go);

        }

        public override void Execute(Command command)
        {
            throw new NotImplementedException();
        }

        public override bool Validate(Command command)
        {
            throw new NotImplementedException();
        }

        public override void ValidateCurrentState()
        {
            throw new NotImplementedException();
        }
    }


}
