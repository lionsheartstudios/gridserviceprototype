﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace SCServer.SCClasses
{
    class State {
        public List<GridObject> buildings { get; set; }
    }

    public static class ModelRegistry
    {
        public static Dictionary<string, Model> objectRegistry = new Dictionary<string, Model>();

        public static void Init(String modelState){
            Console.WriteLine("INIT REGISTRY from "+modelState);

        }

        internal static void AddModel(Model model)
        {
            objectRegistry.Add(model.Id, model);
        }

        private static void RemoveModel(string id)
        {
            objectRegistry.Remove(id);
        }

        public static Model GetModel(string modelId)
        {
            // @aleksa, .GetValueOrDefault this is not compiling for some reason
            return null; //objectRegistry.GetValueOrDefault(modelId);
        }

    }
}
