const { spawn } = require('child_process');

/**
 * This class represents an Island, which is effectively a Grid with Objects on it.
 * It will be constructed by the GameServer Gateway, which would provide a GridId to the constructor().
 * The constructor is spins a new shell process, which runs the SCServerConsoleApp. Hence, we get one process per Grid.
 * All websocket messages that are sent to this Grid would be passed to the SCServerConsoleApp process,
 * and all responses from the process would be relayed back (or broadcasted) to all clients connected to this gridId.
 */
module.exports = class Grid {

    /**
     * construct a Grid. Upon receiving a result from the SCServerConsoleApp (error or otherwise),
     * call the provided broadcast() function to broadcast the result to all clients in the given gridId
     * @param gridId
     * @param broadcast
     */
    constructor(gridId, broadcast){
        this.gridId = gridId;

        this.process = spawn(`${__dirname}/runSCServer.sh`);

        this.process.stdout.on('data', (stdout) => {
            console.log(`${gridId} stdout: ${stdout}`);
            broadcast(gridId, stdout.toString());
        });

        this.process.stderr.on('data', (stderr) => {
            console.log(`${gridId} stderr: ${stderr}`);
            broadcast(gridId, {error: stderr.toString()});
        });

    }

    /**
     * Send the passed command to the SCServerConsoleApp process
     * @param command
     */
    sendToSCServer(command){
        this.process.stdin.write(`${command}\n`);
    }

}
