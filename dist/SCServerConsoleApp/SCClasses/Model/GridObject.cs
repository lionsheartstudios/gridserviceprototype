﻿using System;
namespace SCServer.SCClasses
{
    public abstract class GridObject : Model
    {
        public int x, z, sizeX, sizeZ;
        public bool isSuperimposable = false;

        public GridObject(string _id) : base(_id)
        {
        }
    }
}
