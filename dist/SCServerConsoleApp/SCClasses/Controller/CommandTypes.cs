﻿using System;
namespace SCServer.SCClasses
{
    public static class CommandTypes
    {
        public const string MOVE = "MOVE";
        public const string REMOVE = "REMOVE";
        public const string SET_STATE = "SET_STATE";
    }
}
