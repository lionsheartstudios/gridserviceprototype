const express = require('express');
const app = express();
const url = require('url');
const WebSocketServer = require('ws').Server;
const Grid = require('./Grid');

// a set of gridId->[WebSocket], ie, a set of all WebSocket clients that are on a given Grid (island)
let WebSocketTable = {};
// a set of gridId->Grid
let GridTable = {};

// start the websocket server
const wss = new WebSocketServer({port: 4000});

// send the given message to all clients connected against the given gridId
const broadcast = (gridId, message) => {
    if(message && typeof message === 'object')
        message = JSON.stringify(message);
    WebSocketTable[gridId].forEach( _ws => _ws.send(message) )
}

// if a new client connection comes with a gridId, we add it to the WebSocketTable against the given gridId.
// hence, the client has 'joined an island'.
// if not gridId was provided, we create a new Grid (island) and associate the client with it.
wss.on('connection', (ws, req) => {

    let gridId = null;
    let query = {};
    let _query = url.parse(req.url).query;
    if(_query) {
        _query.split('&').forEach(tok=>{
            var tok = tok.split('=');
            query[tok[0]] = tok[1];
        });
        gridId = query.gridId;
    }

    // if the request came with a gridId
    if(gridId) {
        // if the provided gridId doesnt exist, error
        if (!WebSocketTable[gridId]) {
            ws.send(JSON.stringify({error: `invalid gridId ${gridId}`}));
            ws.close();
            return;
        }
        // join the grid
        WebSocketTable[gridId].push(ws);
    }

    // if the request didnt come with a gridId, create one
    // @richard: this would perhaps never happen, since grid creation is done by GameServer
    if(!gridId) {
        // create a new Grid by generating a new gridId. The second param in the Grid constructor is a broadcast handler
        gridId = Math.floor(Math.random()*90000) + 10000;
        GridTable[gridId] = new Grid(gridId, broadcast);
        // and add the websocket to the WebSocketTable
        WebSocketTable[gridId] = [ws];
    }

    // finally, broadcast that a new client has joined in to everyone in the group
    broadcast(gridId, {status:"CLIENT_CONNECTED", gridId:gridId, noOfClients:WebSocketTable[gridId].length})

    ws.on('message', function (message) {
        console.log('received: %s', message);
        // the message mush contain the gridId
        GridTable[gridId].sendToSCServer(message);
    });

    ws.on('close', function close() {
        WebSocketTable[gridId].splice(WebSocketTable[gridId].indexOf(ws), 1);
        broadcast(gridId, {status:"CLIENT_DISCONNECTED", gridId:gridId, noOfClients:WebSocketTable[gridId].length})
    });

})


































